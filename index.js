const express = require('express')
const app = express()
const helmet = require('helmet');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const config = require('./config')
const port = config.server.port
const passport = require('passport')

app.use(helmet());
app.use(require('morgan')('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ type: "application/json" }) );
app.use(cookieParser());
app.use(express.static(__dirname + '/public'));
app.use(passport.initialize())
app.use(require('./routes'));



var server = app.listen(port, () => {
	console.log('Listening on port ' + server.address().port);
});