var router = require('express').Router();
const path = require('path')
const passport = require('passport')
const passport_init = require("./../passport/custom_auth")
const verify_auth = require('./../lib/verify_auth')

passport_init();

router.get('/',verify_auth,(req, res) => res.sendFile(path.resolve("./public/html/home.html")))

router.get('/login',(req,res)=> res.sendFile(path.resolve("./public/html/login.html")))

router.get('/signup',(req,res)=> res.sendFile(path.resolve("./public/html/signup.html")))


router.get('/logout',(req,res)=>{
	res.clearCookie('token');
	res.redirect('/login');
})


router.post('/signup',
	passport.authenticate('signup',{session:false, failureRedirect:"/signup"}),
	(req,res)=>{
		res.redirect('/login')
	}
)

router.post(
	'/login',
	passport.authenticate('login', {session: false,failureRedirect: '/login'}),
	(req,res)=>{
		if(req.token){
			res.cookie("token",req.token);
		}
		res.redirect('/')
	}
)

router.use("/api",require('./api'))


router.all("*",(req, res) => {
	res.sendStatus(404);
})



module.exports = router;