const passport = require('passport');
const passportCustom = require('passport-custom');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config=require('./../config');
const user = require('./../dbschema/user');

const CustomStrategy = passportCustom.Strategy;

let init = function(){

    // Login Strategy
    passport.use('login',new CustomStrategy(
        (req,callback) =>{
            if(req.body){
                if(req.body.username && req.body.password){
                    
                    user.find({username:req.body.username},(err,docs)=>{
                        if(!err){
                            if(docs.length >= 1){
                                if(docs[0].username  == req.body.username){
                                    bcrypt.compare(req.body.password,docs[0].password, function(err, result) {
                                        if(!err){
                                            if(result){
                                                var token = jwt.sign({"user" : "admin"}, config.jwt.secret);
                                                req.token = token
                                                callback(null,true)
                                            }
                                            else{
                                                console.log("passport-login : password not matching.")
                                                callback(null,false)
                                            }

                                        }
                                        else{
                                            console.log("passport-login : password check failed.")
                                            callback(null,false)
                                        }
                                    });
                                        
                                }
                                else 
                                {
                                    console.log("passport-login : username not matching")
                                    callback(null,false)
                                }  
                                    
                            }
                            else
                            {
                                console.log("passport-login : no user document found")
                                callback(null,false)
                            }
                                
                        }
                        else{
                            console.log("passport-login : db error occured")
                            callback(null,false)
                        }
                            
                    })
                    
                   
                }
                else{
                    console.log("passport-login : username and password required !")
                    callback(null,false)
                }
                    
            }
            else{
                console.log("passport-login : no body found")
                callback(null,false)
            }
            
        }
    ))


    // Signup Strategy
    passport.use('signup',new CustomStrategy(
        (req,callback)=>{
            if(req.body){

                if(req.body.username && req.body.password){
                    var new_user = new user({
                        username : req.body.username,
                        password : req.body.password, 
                    })
                    if("email" in req.body){
                        new_user.email = req.body.email
                    }
                    // check user existence
                    user.find({username:req.body.username},(err,docs)=>{
                        if(!err){
                            if(docs.length <= 0){
                                bcrypt.hash(req.body.password, config.bcrypt.salt).then(function(hash) {
                                    new_user.password = hash
                                    new_user.save().then((err1)=>{
                                        if(!err){
                                           
                                            console.log(req.body.username + " user created")
                                            callback(null,true)
                                
                                        }
                                        else{
                                            console.log("passport-signup :  User saving Failed -" +err1)
                                            callback(null,false)
                                        }
                                    })
                                    

                                });
                                
                                
                            }
                            else{
                                console.log("passport-signup : User already exists")
                                callback(null,false)
                            }
                        }
                        else{
                            console.log("passport-signup :  User existence check failed -" + err)
                            callback(null,false)
                        }
                    })


                }
                else{
                    console.log("passport-signup :  Username and password required !")
                    callback(null,false)
                }
            }
            else{
                console.log("passport-signup :  No body found")
                callback(null,false)
            }
        }
    ))
}

module.exports = init